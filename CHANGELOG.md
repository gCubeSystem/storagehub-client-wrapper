# Changelog for storagehub-client-wrapper

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.2] - 2024-09-17

- [#27898] Updated `uploadFile` and `uploadArchive` methods. They include the `fileSize` parameter required by SHUB.2.X
- [#28026] Moved to `maven-portal-bom 4.0.0{-SNAPSHOT}`

## [v1.2.1] - 2023-04-04

- [#24597] Fixed no notification sent for items updated in the VRE Folder with score char in the name

## [v1.2.0] - 2022-05-02

#### Enhancements

- [#23225] Updated the method to read the members of (VRE or Simple) shared folders

## [v1-1-0] - 2021-05-12

#### Enhancements

[#21412] Added some methods that were missing


## [v1-0-1-SNAPSHOT] - 2021-03-03

Improved JUnit Test


## [v1-0-0]- 2020-07-15

#### Enhancements

[#19317] component moved from 0.y.z to 1.y.z version

[#19668] add the method updateDescriptionForItem


## [v0-7-1]  - 2020-05-18

#### Enhancements

[#19058] added the folder destination Id to restore operation


## [v0-7-0] - 2020-04-16

#### Enhancements

[#19087] provide a getItem (without getRoot inside) for Workspace-Explorer-App



## [v0-6-2] - 2020-03-11

#### New features

[#18174] Moved to new search facility provided by SHUB



## [v0-6-1] - 2019-12-19

Ported to Git and Jenkins



## [v0-6-0] - 2019-10-01

[Task #16688] Integrating new method added into SHUB



## [v0-5-0] - 2019-08-01

Released due to exceptions thrown by SHUB



## [v0-4-0] - 2019-06-01

Updated to new SHub interface

Added method getMetadata



## [v0-3-0] - 2018-03-01

[Task #12059] added delete item

[Task #12533] added trash operations

[Task #12556] added download facility

[Task #12601] added download folder facility

[Task #12604] added Move operation to StorageHub

[Task #12603] added Copy operation to StorageHub

[Task #12603] added Rename facility to StorageHub

[Task #12603] added Public Link facility to StorageHub

[Task #12664] added Versions facility to StorageHub

[Task #12720] added Image Preview to StorageHub



## [v0-2-0] - 2018-06-20

minor fixes



## [v0-1-0] - 2018-06-20

[Task #12059] first release

