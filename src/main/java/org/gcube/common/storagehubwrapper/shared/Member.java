package org.gcube.common.storagehubwrapper.shared;

import java.io.Serializable;

/**
 * The Class Member.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 29, 2022
 */
public class Member implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1454948336452658186L;

	/**
	 * The Enum TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Apr 29, 2022
	 */
	public static enum TYPE {
		USER, GROUP
	}

	private String identity;
	private String name;
	private TYPE memberType = TYPE.USER;

	/**
	 * Instantiates a new member.
	 */
	public Member() {

	}

	/**
	 * Instantiates a new member.
	 *
	 * @param identity   the identity is the username of a User or the name of a
	 *                   Group. In general is key to identify he/she/it in the in
	 *                   the infrastructure
	 * @param name       the name is the name of a User or the name of a Group
	 * @param memberType the member type
	 */
	public Member(String identity, String name, TYPE memberType) {
		super();
		this.identity = identity;
		this.name = name;
		this.memberType = memberType;
	}

	/**
	 * Gets the identity.
	 *
	 * @return the login in case of user and the groupName stored in SHUB (e.g.
	 *         gcube-devsec-devVRE) in case of group
	 */
	public String getIdentity() {
		return identity;
	}

	/**
	 * Gets the name.
	 *
	 * @return the login in case of user and the groupName in case of group
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the member type.
	 *
	 * @return the member type
	 */
	public TYPE getMemberType() {
		return memberType;
	}

	/**
	 * Sets the identity.
	 *
	 * @param identity the new identity
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the member type.
	 *
	 * @param memberType the new member type
	 */
	public void setMemberType(TYPE memberType) {
		this.memberType = memberType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Member [identity=");
		builder.append(identity);
		builder.append(", name=");
		builder.append(name);
		builder.append(", memberType=");
		builder.append(memberType);
		builder.append("]");
		return builder.toString();
	}

}
