/**
 *
 */

package org.gcube.common.storagehubwrapper.shared.tohl;


/**
 * The Enum WorkspaceItemType.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jul 29, 2019
 */
public enum WorkspaceItemType {
	/**
	 * A folder.
	 */
	FOLDER,
	/**
	 * A shared folder
	 */
	SHARED_FOLDER,
	/**
	 * A smart folder
	 */
	SMART_FOLDER,
	/**
	 * The vre folder.
	 */
	VRE_FOLDER,
	/**
	 * A folder item.
	 */
	FILE_ITEM,
	/**
	 * A trash folder.
	 */
	TRASH_FOLDER,
	/**
	 * A trash item.
	 */
	TRASH_ITEM,
	
	
	/** The url item. */
	URL_ITEM
}
