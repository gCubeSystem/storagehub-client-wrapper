/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.items;

import java.net.URL;

import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceItem;


/**
 * The Interface URLItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jul 29, 2019
 */
public interface URLItem extends WorkspaceItem{
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public URL getValue();


}
