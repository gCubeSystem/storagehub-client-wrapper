/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.items;

/**
 * The Interface URLItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jul 29, 2019
 * 
 * to backward compatibility with HL
 */
public interface URLFileItem extends FileItem{
	

}
