///**
// *
// */
//package org.gcube.common.storagehubwrapper.shared.tohl;
//
//import java.util.Calendar;
//
//import org.gcube.common.storagehub.model.items.nodes.accounting.AccountingEntryType;
//
///**
// * The Interface AccountingEntry.
// *
// * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
// * Jun 15, 2018
// */
//public interface AccountingEntry {
//
//	/**
//	 * Gets the user.
//	 *
//	 * @return the user
//	 */
//	String getUser();
//
//	/**
//	 * Gets the date.
//	 *
//	 * @return the date
//	 */
//	Calendar getDate();
//
//	/**
//	 * Gets the entry type.
//	 *
//	 * @return the entry type
//	 */
//	AccountingEntryType getEntryType();
//
//	/**
//	 * Gets the version.
//	 *
//	 * @return the version
//	 */
//	String getVersion();
//}
