/**
 *
 */
package org.gcube.common.storagehubwrapper.shared.tohl.impl;

/**
 * The Class URLFileItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jul 29, 2019
 * 
 * to backward compatibility with HL
 */
public class URLFileItem extends FileItem implements org.gcube.common.storagehubwrapper.shared.tohl.items.URLFileItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = -369007015284469987L;

}
