/**
 *
 */

package org.gcube.common.storagehubwrapper.server;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class WrapperUtility.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Oct 17, 2018
 */
public class WrapperUtility {
	
	public static final String SCOPE_SEPARATOR = "/";

	private static Logger logger = LoggerFactory.getLogger(WrapperUtility.class);

	/**
	 * To input stream.
	 *
	 * @param content the content
	 * @return the input stream
	 */
	public static InputStream toInputStream(byte[] content) {

		int size = content.length;
		InputStream is = null;
		byte[] b = new byte[size];
		try {
			is = new ByteArrayInputStream(content);
			is.read(b);
			return is;
		}
		catch (IOException e) {
			logger.warn("ToInputStream error: ",e);
			return null;
		}
		finally {
			try {
				if (is != null)
					is.close();
			}
			catch (Exception ex) {
			}
		}
	}
	
	/**
	 * Gets the infrastructure name from scope.
	 *
	 * @param scope the scope
	 * @return the infrastructure name from scope
	 * @throws Exception the exception
	 */
	public static String getInfrastructureNameFromScope(String scope) throws Exception{

		if(scope==null || scope.isEmpty()){
			throw new Exception("Scope is null or empty");
		}

		if(!scope.startsWith(SCOPE_SEPARATOR)){
			logger.warn("Input scope: "+scope+" not have / is a really scope?");
			scope = SCOPE_SEPARATOR+scope;
			logger.warn("Tentative as scope: "+scope);
		}

		String[] splitScope = scope.split(SCOPE_SEPARATOR);

		String rootScope = SCOPE_SEPARATOR + splitScope[1];

		if(rootScope.length()<2){
			throw new Exception("Infrastructure name not found in "+scope);
		}

		return rootScope;

	}
}
